'use strict';

customElements.define('compodoc-menu', class extends HTMLElement {
    constructor() {
        super();
        this.isNormalMode = this.getAttribute('mode') === 'normal';
    }

    connectedCallback() {
        this.render(this.isNormalMode);
    }

    render(isNormalMode) {
        let tp = lithtml.html(`
        <nav>
            <ul class="list">
                <li class="title">
                    <a href="index.html" data-type="index-link">mynitro-api documentation</a>
                </li>

                <li class="divider"></li>
                ${ isNormalMode ? `<div id="book-search-input" role="search"><input type="text" placeholder="Type to search"></div>` : '' }
                <li class="chapter">
                    <a data-type="chapter-link" href="index.html"><span class="icon ion-ios-home"></span>Getting started</a>
                    <ul class="links">
                        <li class="link">
                            <a href="overview.html" data-type="chapter-link">
                                <span class="icon ion-ios-keypad"></span>Overview
                            </a>
                        </li>
                        <li class="link">
                            <a href="index.html" data-type="chapter-link">
                                <span class="icon ion-ios-paper"></span>README
                            </a>
                        </li>
                                <li class="link">
                                    <a href="dependencies.html" data-type="chapter-link">
                                        <span class="icon ion-ios-list"></span>Dependencies
                                    </a>
                                </li>
                                <li class="link">
                                    <a href="properties.html" data-type="chapter-link">
                                        <span class="icon ion-ios-apps"></span>Properties
                                    </a>
                                </li>
                    </ul>
                </li>
                    <li class="chapter modules">
                        <a data-type="chapter-link" href="modules.html">
                            <div class="menu-toggler linked" data-toggle="collapse" ${ isNormalMode ?
                                'data-target="#modules-links"' : 'data-target="#xs-modules-links"' }>
                                <span class="icon ion-ios-archive"></span>
                                <span class="link-name">Modules</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                        </a>
                        <ul class="links collapse " ${ isNormalMode ? 'id="modules-links"' : 'id="xs-modules-links"' }>
                            <li class="link">
                                <a href="modules/AppModule.html" data-type="entity-link" >AppModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DatabaseModule.html" data-type="entity-link" >DatabaseModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DatabaseModule.html" data-type="entity-link" >DatabaseModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/DeviceModule.html" data-type="entity-link" >DeviceModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-DeviceModule-7c68f66b944f9ac4a0dc162659e1499712f3de416970caa7d95baf2660e83ca8e765a2d8fcd9a1971f13bf2f183ddc6953d236440f8508a3bd4b6287e04ebf68"' : 'data-target="#xs-controllers-links-module-DeviceModule-7c68f66b944f9ac4a0dc162659e1499712f3de416970caa7d95baf2660e83ca8e765a2d8fcd9a1971f13bf2f183ddc6953d236440f8508a3bd4b6287e04ebf68"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-DeviceModule-7c68f66b944f9ac4a0dc162659e1499712f3de416970caa7d95baf2660e83ca8e765a2d8fcd9a1971f13bf2f183ddc6953d236440f8508a3bd4b6287e04ebf68"' :
                                            'id="xs-controllers-links-module-DeviceModule-7c68f66b944f9ac4a0dc162659e1499712f3de416970caa7d95baf2660e83ca8e765a2d8fcd9a1971f13bf2f183ddc6953d236440f8508a3bd4b6287e04ebf68"' }>
                                            <li class="link">
                                                <a href="controllers/DeviceController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DeviceController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-DeviceModule-7c68f66b944f9ac4a0dc162659e1499712f3de416970caa7d95baf2660e83ca8e765a2d8fcd9a1971f13bf2f183ddc6953d236440f8508a3bd4b6287e04ebf68"' : 'data-target="#xs-injectables-links-module-DeviceModule-7c68f66b944f9ac4a0dc162659e1499712f3de416970caa7d95baf2660e83ca8e765a2d8fcd9a1971f13bf2f183ddc6953d236440f8508a3bd4b6287e04ebf68"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-DeviceModule-7c68f66b944f9ac4a0dc162659e1499712f3de416970caa7d95baf2660e83ca8e765a2d8fcd9a1971f13bf2f183ddc6953d236440f8508a3bd4b6287e04ebf68"' :
                                        'id="xs-injectables-links-module-DeviceModule-7c68f66b944f9ac4a0dc162659e1499712f3de416970caa7d95baf2660e83ca8e765a2d8fcd9a1971f13bf2f183ddc6953d236440f8508a3bd4b6287e04ebf68"' }>
                                        <li class="link">
                                            <a href="injectables/DeviceService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >DeviceService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/OperatorModule.html" data-type="entity-link" >OperatorModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-OperatorModule-db30c8c22900dbd681d1dfa2ecddb923a1e4437878d77b8724b94f1faff342292657588bdea8750ed6087bdc6a7e435805b171938df5d4565e93416e98fc0cbb"' : 'data-target="#xs-controllers-links-module-OperatorModule-db30c8c22900dbd681d1dfa2ecddb923a1e4437878d77b8724b94f1faff342292657588bdea8750ed6087bdc6a7e435805b171938df5d4565e93416e98fc0cbb"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-OperatorModule-db30c8c22900dbd681d1dfa2ecddb923a1e4437878d77b8724b94f1faff342292657588bdea8750ed6087bdc6a7e435805b171938df5d4565e93416e98fc0cbb"' :
                                            'id="xs-controllers-links-module-OperatorModule-db30c8c22900dbd681d1dfa2ecddb923a1e4437878d77b8724b94f1faff342292657588bdea8750ed6087bdc6a7e435805b171938df5d4565e93416e98fc0cbb"' }>
                                            <li class="link">
                                                <a href="controllers/OperatorController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >OperatorController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-OperatorModule-db30c8c22900dbd681d1dfa2ecddb923a1e4437878d77b8724b94f1faff342292657588bdea8750ed6087bdc6a7e435805b171938df5d4565e93416e98fc0cbb"' : 'data-target="#xs-injectables-links-module-OperatorModule-db30c8c22900dbd681d1dfa2ecddb923a1e4437878d77b8724b94f1faff342292657588bdea8750ed6087bdc6a7e435805b171938df5d4565e93416e98fc0cbb"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-OperatorModule-db30c8c22900dbd681d1dfa2ecddb923a1e4437878d77b8724b94f1faff342292657588bdea8750ed6087bdc6a7e435805b171938df5d4565e93416e98fc0cbb"' :
                                        'id="xs-injectables-links-module-OperatorModule-db30c8c22900dbd681d1dfa2ecddb923a1e4437878d77b8724b94f1faff342292657588bdea8750ed6087bdc6a7e435805b171938df5d4565e93416e98fc0cbb"' }>
                                        <li class="link">
                                            <a href="injectables/OperatorService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >OperatorService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/OrderModule.html" data-type="entity-link" >OrderModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-OrderModule-c0cc01f3921ae843db95f90cac900a68016904fe84f344d8d72574e2729051086c23a5283cafa409bf0479e1a072919da60b4bbfc5e6a7ad82737305e2ed88aa"' : 'data-target="#xs-controllers-links-module-OrderModule-c0cc01f3921ae843db95f90cac900a68016904fe84f344d8d72574e2729051086c23a5283cafa409bf0479e1a072919da60b4bbfc5e6a7ad82737305e2ed88aa"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-OrderModule-c0cc01f3921ae843db95f90cac900a68016904fe84f344d8d72574e2729051086c23a5283cafa409bf0479e1a072919da60b4bbfc5e6a7ad82737305e2ed88aa"' :
                                            'id="xs-controllers-links-module-OrderModule-c0cc01f3921ae843db95f90cac900a68016904fe84f344d8d72574e2729051086c23a5283cafa409bf0479e1a072919da60b4bbfc5e6a7ad82737305e2ed88aa"' }>
                                            <li class="link">
                                                <a href="controllers/OrderController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >OrderController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-OrderModule-c0cc01f3921ae843db95f90cac900a68016904fe84f344d8d72574e2729051086c23a5283cafa409bf0479e1a072919da60b4bbfc5e6a7ad82737305e2ed88aa"' : 'data-target="#xs-injectables-links-module-OrderModule-c0cc01f3921ae843db95f90cac900a68016904fe84f344d8d72574e2729051086c23a5283cafa409bf0479e1a072919da60b4bbfc5e6a7ad82737305e2ed88aa"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-OrderModule-c0cc01f3921ae843db95f90cac900a68016904fe84f344d8d72574e2729051086c23a5283cafa409bf0479e1a072919da60b4bbfc5e6a7ad82737305e2ed88aa"' :
                                        'id="xs-injectables-links-module-OrderModule-c0cc01f3921ae843db95f90cac900a68016904fe84f344d8d72574e2729051086c23a5283cafa409bf0479e1a072919da60b4bbfc5e6a7ad82737305e2ed88aa"' }>
                                        <li class="link">
                                            <a href="injectables/OrderService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >OrderService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/PaymentGatewayModule.html" data-type="entity-link" >PaymentGatewayModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-PaymentGatewayModule-0310781f4afc9d73d335467053806f012b8815fdbf84ceddc8dfe3268f475e96215048d05951de88a9517fc1ea086f9c70c874e11ac78cee3880068d488ff98e"' : 'data-target="#xs-injectables-links-module-PaymentGatewayModule-0310781f4afc9d73d335467053806f012b8815fdbf84ceddc8dfe3268f475e96215048d05951de88a9517fc1ea086f9c70c874e11ac78cee3880068d488ff98e"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-PaymentGatewayModule-0310781f4afc9d73d335467053806f012b8815fdbf84ceddc8dfe3268f475e96215048d05951de88a9517fc1ea086f9c70c874e11ac78cee3880068d488ff98e"' :
                                        'id="xs-injectables-links-module-PaymentGatewayModule-0310781f4afc9d73d335467053806f012b8815fdbf84ceddc8dfe3268f475e96215048d05951de88a9517fc1ea086f9c70c874e11ac78cee3880068d488ff98e"' }>
                                        <li class="link">
                                            <a href="injectables/PaymentGatewayService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PaymentGatewayService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/PaymentModule.html" data-type="entity-link" >PaymentModule</a>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-PaymentModule-891c982da81cf71d3f7113fc76c77a3cc603de8ad7970548f347f7d79646e3207b672a2b97ce30fe1c8e2fb95ff9b3454bf43619e371157884a4e9b98101b7cf"' : 'data-target="#xs-injectables-links-module-PaymentModule-891c982da81cf71d3f7113fc76c77a3cc603de8ad7970548f347f7d79646e3207b672a2b97ce30fe1c8e2fb95ff9b3454bf43619e371157884a4e9b98101b7cf"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-PaymentModule-891c982da81cf71d3f7113fc76c77a3cc603de8ad7970548f347f7d79646e3207b672a2b97ce30fe1c8e2fb95ff9b3454bf43619e371157884a4e9b98101b7cf"' :
                                        'id="xs-injectables-links-module-PaymentModule-891c982da81cf71d3f7113fc76c77a3cc603de8ad7970548f347f7d79646e3207b672a2b97ce30fe1c8e2fb95ff9b3454bf43619e371157884a4e9b98101b7cf"' }>
                                        <li class="link">
                                            <a href="injectables/PaymentService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >PaymentService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/QrPaymentModule.html" data-type="entity-link" >QrPaymentModule</a>
                            </li>
                            <li class="link">
                                <a href="modules/UserModule.html" data-type="entity-link" >UserModule</a>
                                    <li class="chapter inner">
                                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                            'data-target="#controllers-links-module-UserModule-661b490b8635d9a20c5c5301d0110f96206b5976e5739eeb91ddf967f762a26d5de91a2d7fe560d5cc487de095a7227ebd751019c621774e019845e68424c41b"' : 'data-target="#xs-controllers-links-module-UserModule-661b490b8635d9a20c5c5301d0110f96206b5976e5739eeb91ddf967f762a26d5de91a2d7fe560d5cc487de095a7227ebd751019c621774e019845e68424c41b"' }>
                                            <span class="icon ion-md-swap"></span>
                                            <span>Controllers</span>
                                            <span class="icon ion-ios-arrow-down"></span>
                                        </div>
                                        <ul class="links collapse" ${ isNormalMode ? 'id="controllers-links-module-UserModule-661b490b8635d9a20c5c5301d0110f96206b5976e5739eeb91ddf967f762a26d5de91a2d7fe560d5cc487de095a7227ebd751019c621774e019845e68424c41b"' :
                                            'id="xs-controllers-links-module-UserModule-661b490b8635d9a20c5c5301d0110f96206b5976e5739eeb91ddf967f762a26d5de91a2d7fe560d5cc487de095a7227ebd751019c621774e019845e68424c41b"' }>
                                            <li class="link">
                                                <a href="controllers/UserController.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserController</a>
                                            </li>
                                        </ul>
                                    </li>
                                <li class="chapter inner">
                                    <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ?
                                        'data-target="#injectables-links-module-UserModule-661b490b8635d9a20c5c5301d0110f96206b5976e5739eeb91ddf967f762a26d5de91a2d7fe560d5cc487de095a7227ebd751019c621774e019845e68424c41b"' : 'data-target="#xs-injectables-links-module-UserModule-661b490b8635d9a20c5c5301d0110f96206b5976e5739eeb91ddf967f762a26d5de91a2d7fe560d5cc487de095a7227ebd751019c621774e019845e68424c41b"' }>
                                        <span class="icon ion-md-arrow-round-down"></span>
                                        <span>Injectables</span>
                                        <span class="icon ion-ios-arrow-down"></span>
                                    </div>
                                    <ul class="links collapse" ${ isNormalMode ? 'id="injectables-links-module-UserModule-661b490b8635d9a20c5c5301d0110f96206b5976e5739eeb91ddf967f762a26d5de91a2d7fe560d5cc487de095a7227ebd751019c621774e019845e68424c41b"' :
                                        'id="xs-injectables-links-module-UserModule-661b490b8635d9a20c5c5301d0110f96206b5976e5739eeb91ddf967f762a26d5de91a2d7fe560d5cc487de095a7227ebd751019c621774e019845e68424c41b"' }>
                                        <li class="link">
                                            <a href="injectables/UserService.html" data-type="entity-link" data-context="sub-entity" data-context-id="modules" >UserService</a>
                                        </li>
                                    </ul>
                                </li>
                            </li>
                            <li class="link">
                                <a href="modules/V0Module.html" data-type="entity-link" >V0Module</a>
                            </li>
                </ul>
                </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#entities-links"' :
                                'data-target="#xs-entities-links"' }>
                                <span class="icon ion-ios-apps"></span>
                                <span>Entities</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="entities-links"' : 'id="xs-entities-links"' }>
                                <li class="link">
                                    <a href="entities/Device.html" data-type="entity-link" >Device</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Operator.html" data-type="entity-link" >Operator</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Order.html" data-type="entity-link" >Order</a>
                                </li>
                                <li class="link">
                                    <a href="entities/Payment.html" data-type="entity-link" >Payment</a>
                                </li>
                                <li class="link">
                                    <a href="entities/QrPayment.html" data-type="entity-link" >QrPayment</a>
                                </li>
                                <li class="link">
                                    <a href="entities/User.html" data-type="entity-link" >User</a>
                                </li>
                                <li class="link">
                                    <a href="entities/UserDevice.html" data-type="entity-link" >UserDevice</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#classes-links"' :
                            'data-target="#xs-classes-links"' }>
                            <span class="icon ion-ios-paper"></span>
                            <span>Classes</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="classes-links"' : 'id="xs-classes-links"' }>
                            <li class="link">
                                <a href="classes/BaseDomain.html" data-type="entity-link" >BaseDomain</a>
                            </li>
                            <li class="link">
                                <a href="classes/BaseRequestBody.html" data-type="entity-link" >BaseRequestBody</a>
                            </li>
                            <li class="link">
                                <a href="classes/BaseResponse.html" data-type="entity-link" >BaseResponse</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateDeviceDto.html" data-type="entity-link" >CreateDeviceDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateOperatorDto.html" data-type="entity-link" >CreateOperatorDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateOrderDto.html" data-type="entity-link" >CreateOrderDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreatePaymentDto.html" data-type="entity-link" >CreatePaymentDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateQrPaymentDto.html" data-type="entity-link" >CreateQrPaymentDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/CreateUserDto.html" data-type="entity-link" >CreateUserDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/GetQrRequestBody.html" data-type="entity-link" >GetQrRequestBody</a>
                            </li>
                            <li class="link">
                                <a href="classes/GetQrResponse.html" data-type="entity-link" >GetQrResponse</a>
                            </li>
                            <li class="link">
                                <a href="classes/GetStatusQrRequestBody.html" data-type="entity-link" >GetStatusQrRequestBody</a>
                            </li>
                            <li class="link">
                                <a href="classes/GetStatusQrResponse.html" data-type="entity-link" >GetStatusQrResponse</a>
                            </li>
                            <li class="link">
                                <a href="classes/GetStatusQrResponseMapper.html" data-type="entity-link" >GetStatusQrResponseMapper</a>
                            </li>
                            <li class="link">
                                <a href="classes/IsUniqueConstraints.html" data-type="entity-link" >IsUniqueConstraints</a>
                            </li>
                            <li class="link">
                                <a href="classes/IsUniqueConstraints-1.html" data-type="entity-link" >IsUniqueConstraints</a>
                            </li>
                            <li class="link">
                                <a href="classes/PaginationResponse.html" data-type="entity-link" >PaginationResponse</a>
                            </li>
                            <li class="link">
                                <a href="classes/PriceSet.html" data-type="entity-link" >PriceSet</a>
                            </li>
                            <li class="link">
                                <a href="classes/QrPaymentService.html" data-type="entity-link" >QrPaymentService</a>
                            </li>
                            <li class="link">
                                <a href="classes/RegisterDeviceDto.html" data-type="entity-link" >RegisterDeviceDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateDeviceDto.html" data-type="entity-link" >UpdateDeviceDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UpdateOperatorDto.html" data-type="entity-link" >UpdateOperatorDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserAuthDto.html" data-type="entity-link" >UserAuthDto</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserAuthResponse.html" data-type="entity-link" >UserAuthResponse</a>
                            </li>
                            <li class="link">
                                <a href="classes/UserToken.html" data-type="entity-link" >UserToken</a>
                            </li>
                        </ul>
                    </li>
                        <li class="chapter">
                            <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#injectables-links"' :
                                'data-target="#xs-injectables-links"' }>
                                <span class="icon ion-md-arrow-round-down"></span>
                                <span>Injectables</span>
                                <span class="icon ion-ios-arrow-down"></span>
                            </div>
                            <ul class="links collapse " ${ isNormalMode ? 'id="injectables-links"' : 'id="xs-injectables-links"' }>
                                <li class="link">
                                    <a href="injectables/DefaultSerializePipe.html" data-type="entity-link" >DefaultSerializePipe</a>
                                </li>
                            </ul>
                        </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#guards-links"' :
                            'data-target="#xs-guards-links"' }>
                            <span class="icon ion-ios-lock"></span>
                            <span>Guards</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="guards-links"' : 'id="xs-guards-links"' }>
                            <li class="link">
                                <a href="guards/DeviceGuard.html" data-type="entity-link" >DeviceGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/RolesGuard.html" data-type="entity-link" >RolesGuard</a>
                            </li>
                            <li class="link">
                                <a href="guards/UserGuard.html" data-type="entity-link" >UserGuard</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <div class="simple menu-toggler" data-toggle="collapse" ${ isNormalMode ? 'data-target="#miscellaneous-links"'
                            : 'data-target="#xs-miscellaneous-links"' }>
                            <span class="icon ion-ios-cube"></span>
                            <span>Miscellaneous</span>
                            <span class="icon ion-ios-arrow-down"></span>
                        </div>
                        <ul class="links collapse " ${ isNormalMode ? 'id="miscellaneous-links"' : 'id="xs-miscellaneous-links"' }>
                            <li class="link">
                                <a href="miscellaneous/functions.html" data-type="entity-link">Functions</a>
                            </li>
                            <li class="link">
                                <a href="miscellaneous/variables.html" data-type="entity-link">Variables</a>
                            </li>
                        </ul>
                    </li>
                    <li class="chapter">
                        <a data-type="chapter-link" href="coverage.html"><span class="icon ion-ios-stats"></span>Documentation coverage</a>
                    </li>
                    <li class="divider"></li>
                    <li class="copyright">
                        Documentation generated using <a href="https://compodoc.app/" target="_blank">
                            <img data-src="images/compodoc-vectorise.png" class="img-responsive" data-type="compodoc-logo">
                        </a>
                    </li>
            </ul>
        </nav>
        `);
        this.innerHTML = tp.strings;
    }
});