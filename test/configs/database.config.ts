import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { TypeOrmModule } from '@nestjs/typeorm'
import { User } from '../../src/domain/user/user.domain'
import { SnakeNamingStrategy } from 'typeorm-naming-strategies'
import { Device } from '../../src/domain/device/device.domain'
import { UserDevice } from '../../src/domain/user-device/user-device.domain'
import { Operator } from '../../src/domain/operator/operator.domain'

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      useFactory: (env: ConfigService) => ({
        type: 'postgres',
        host: env.get<string>('DB_HOST'),
        port: env.get<number>('DB_PORT'),
        database: env.get<string>('DB_DATABASE') + '-test',
        username: env.get<string>('DB_USERNAME'),
        password: env.get<string>('DB_PASSWORD'),
        synchronize: true,
        entities: [User, Device, UserDevice, Operator],
        namingStrategy: new SnakeNamingStrategy(),
      }),
      imports: [ConfigModule],
      inject: [ConfigService],
    }),
  ],
})
export class DatabaseModule {}
