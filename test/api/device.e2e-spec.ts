import {
  BadRequestException,
  HttpStatus,
  INestApplication,
  ValidationError,
  ValidationPipe,
} from '@nestjs/common'
import { Test, TestingModule } from '@nestjs/testing'
import { DeviceModule } from '../../src/application/api/v0/device/device.module'
import { Device } from '../../src/domain/device/device.domain'
import { randomString } from '../../src/utils/helpers/random-string'
import * as supertest from 'supertest'
import { DatabaseModule } from '../configs/database.config'
import { login } from '../utils/login'
import { truncate } from '../utils/truncate'
import { Factory } from 'typeorm-factory'
import { CreateDeviceDto } from '../../src/application/api/v0/device/device.dto'

describe('Device Routes', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [DeviceModule, DatabaseModule],
    }).compile()

    app = moduleFixture.createNestApplication()
    app.useGlobalPipes(
      new ValidationPipe({
        exceptionFactory: (errors: ValidationError[]) => {
          throw new BadRequestException(errors)
        },
      }),
    )
    await app.init()
  })

  beforeEach(async () => {
    await truncate()
  })

  describe('GET /device', () => {
    it('Should returns all devices', async () => {
      const devices = await new Factory(Device)
        .sequence('imei', () => randomString('aA0', 12))
        .sequence('name', (i) => `Device ${i}`)
        .sequence('secretKey', (i) => `Device-${i}`)
        .sequence('priceSet', (i) => {
          return {
            bikeNew: 1,
            bikePatch: 1,
            bikeRefill: 1,
            carNew: 1,
            carPatch: 1,
            carRefill: 1,
          }
        })
        .createList(3)

      const token = login()
      const res = await supertest(app.getHttpServer())
        .get('/v0/dashboard/device')
        .query({
          limit: 10,
          page: 1,
        })
        .set({
          Authorization: `Bearer ${token}`,
        })
        .expect(HttpStatus.OK)

      expect(res.body).toMatchObject({
        perPage: 10,
        lastPage: expect.any(Number),
        total: expect.any(Number),
        data: expect.any(Array),
      })
    })
  })

  describe('GET /device/:id', () => {
    it('should return device with specific id', async () => {
      const device = await new Factory(Device).create({
        imei: randomString('a', 12),
        name: 'baru',
        secretKey: 'password',
      })

      const token = login()

      const res = await supertest(app.getHttpServer())
        .get(`/v0/dashboard/device/${device.id}`)
        .set({
          Authorization: `Bearer ${token}`,
        })

      expect(res.body).toMatchObject({
        id: expect.any(Number),
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
        deletedAt: null,
        imei: expect.any(String),
        name: expect.any(String),
        address: null,
        latitude: null,
        longitude: null,
        secretKey: expect.any(String),
        qrPaymentTid: null,
        priceSet: {},
      })
    })
  })

  describe('POST /device', () => {
    it('should create a new device', async () => {
      const payload: CreateDeviceDto = {
        name: 'device baru',
        imei: randomString('0a', 12),
        address: 'kosan',
        qrPaymentTid: 'UKS',
        latitude: 0,
        longitude: 0,
        priceSet: {
          bikeNew: 1,
          bikePatch: 1,
          bikeRefill: 1,
          carNew: 1,
          carPatch: 1,
          carRefill: 1,
        },
      }

      const token = `Bearer ${login()}`
      const res = await supertest(app.getHttpServer())
        .post('/v0/dashboard/device')
        .set({
          Authorization: token,
        })
        .send(payload)
        .expect(HttpStatus.CREATED)

      expect(res.body).toMatchObject({
        ...payload,
      })
    })
  })

  describe('POST /device/register', () => {
    it('should register device with by imei', async () => {
      const payload = {
        imei: randomString('a0', 12),
      }

      const res = await supertest(app.getHttpServer())
        .post('/v0/device/register')
        .send(payload)
        .expect(HttpStatus.CREATED)

      expect(res.body).toMatchObject({
        rawSecretKey: expect.any(String),
      })
    })

    it(`should return 'Device Has Been Deleted'`, async () => {
      const token = login()
      const device = await new Factory(Device).create({
        imei: randomString('a0', 12),
        secretKey: randomString('a0', 12),
        deletedAt: new Date(),
      })

      const res = await supertest(app.getHttpServer())
        .post('/v0/device/register')
        .set({ Authorization: `Bearer ${token}` })
        .send({ imei: device.imei })

      expect(res.status).toBe(422)
      expect(res.body).toMatchObject({
        statusCode: 422,
        message: 'Device Has Been Deleted',
        error: 'Unprocessable Entity',
      })
    })
  })

  describe('PUT /device/:id', () => {
    it('should update device', async () => {
      const token = login()

      const device = await new Factory(Device).create({
        name: 'device baru',
        imei: randomString('0a', 12),
        secretKey: 'password',
        address: 'kosan',
        qrPaymentTid: 'UKS',
        latitude: 0,
        longitude: 0,
        priceSet: {
          bikeNew: 1,
          bikePatch: 1,
          bikeRefill: 1,
          carNew: 1,
          carPatch: 1,
          carRefill: 1,
        },
      })

      const res = await supertest(app.getHttpServer())
        .put('/v0/dashboard/device/' + device.id)
        .set({ Authorization: `Bearer ${token}` })
        .send({
          id: device.id,
          name: 'baru update',
        })

      console.log(res.body)
    })
  })

  describe('DELETE /device/:id', () => {
    it('should soft delete device', async () => {
      const token = login()

      const device = await new Factory(Device).create({
        imei: randomString('a0', 12),
        secretKey: randomString('a0', 12),
      })

      const res = await supertest(app.getHttpServer())
        .delete(`/v0/dashboard/device/${device.id}`)
        .set({ Authorization: `Bearer ${token}` })

      expect(res.body).toMatchObject({
        status: true,
      })
    })
  })
})
