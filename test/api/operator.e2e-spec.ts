import {
  BadRequestException,
  HttpStatus,
  INestApplication,
  ValidationError,
  ValidationPipe,
} from '@nestjs/common'
import { Test, TestingModule } from '@nestjs/testing'
import { Device } from '../../src/domain/device/device.domain'
import { Factory } from 'typeorm-factory'
import { CreateOperatorDto } from '../../src/application/api/v0/operator/operator.dto'
import { OperatorModule } from '../../src/application/api/v0/operator/operator.module'
import { DatabaseModule } from '../configs/database.config'
import { truncate } from '../utils/truncate'
import { randomString } from '../../src/utils/helpers/random-string'
import * as supertest from 'supertest'
import { login } from '../utils/login'
import { Operator } from '../../src/domain/operator/operator.domain'

describe('Operator Routes', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [OperatorModule, DatabaseModule],
    }).compile()

    app = moduleFixture.createNestApplication()
    app.useGlobalPipes(
      new ValidationPipe({
        exceptionFactory: (errors: ValidationError[]) => {
          throw new BadRequestException(errors)
        },
      }),
    )
    await app.init()
  })

  beforeEach(async () => {
    await truncate()
  })

  describe('POST /operator', () => {
    it('should create a new operator', async () => {
      const device = await new Factory(Device).create({
        name: 'device baru',
        imei: randomString('0a', 12),
        secretKey: randomString('0aA', 12),
        address: 'kosan',
        qrPaymentTid: 'UKS',
        latitude: 0,
        longitude: 0,
        priceSet: {
          bikeNew: 1,
          bikePatch: 1,
          bikeRefill: 1,
          carNew: 1,
          carPatch: 1,
          carRefill: 1,
        },
      })
      const payload: CreateOperatorDto = {
        deviceId: device.id,
        fullname: 'Operator Baru',
        phone: '088239923809',
      }
      const token = login()

      const res = await supertest(app.getHttpServer())
        .post('/v0/dashboard/operator')
        .send({ ...payload })
        .set({
          Authorization: `Bearer ${token}`,
        })
        .expect(HttpStatus.CREATED)

      expect(res.body).toMatchObject({
        id: expect.any(Number),
        fullname: payload.fullname,
        phone: payload.phone,
        deviceId: device.id,
        pin: expect.any(String),
      })
    })
  })

  describe('GET /operator', () => {
    it('should retrieve all operators data', async () => {
      const token = login()
      const query = {
        limit: 10,
        page: 1,
      }

      const devices = await new Factory(Device)
        .sequence('name', (i) => `device ${i}`)
        .sequence('imei', (i) => `${randomString('a0A', 11)}${i}`)
        .createList(3, {
          secretKey: randomString('0aA', 12),
          address: 'kosan',
          qrPaymentTid: 'UKS',
          latitude: 0,
          longitude: 0,
          priceSet: {
            bikeNew: 1,
            bikePatch: 1,
            bikeRefill: 1,
            carNew: 1,
            carPatch: 1,
            carRefill: 1,
          },
        })

      const operator = await Promise.all(
        devices.map((device) =>
          new Factory(Operator)
            .sequence('fullname', (i) => `Operator ${i}`)
            .sequence('phone', (i) => `08823823989239${i}`)
            .create({
              deviceId: device.id,
              pin: randomString('0', 6),
            }),
        ),
      )

      const res = await supertest(app.getHttpServer)
        .get('/v0/dashboard/operator')
        .query(query)
        .set({ Authorization: `Bearer ${token}` })

      console.log(res.body)
    })
  })
})
