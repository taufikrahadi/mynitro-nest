import { HttpStatus, INestApplication, ValidationPipe } from '@nestjs/common'
import { Test, TestingModule } from '@nestjs/testing'
import { UserModule } from '../../src/application/api/v0/user/user.module'
import { DatabaseModule } from '../configs/database.config'
import { truncate } from '../utils/truncate'
import { Factory } from 'typeorm-factory'
import { User } from '../../src/domain/user/user.domain'
import * as supertest from 'supertest'
import { login } from '../utils/login'
import { APP_PIPE } from '@nestjs/core'

describe('Dashboard User Routes', () => {
  let app: INestApplication

  beforeAll(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [UserModule, DatabaseModule],
      providers: [
        {
          provide: APP_PIPE,
          useClass: ValidationPipe,
        },
      ],
    }).compile()

    app = moduleFixture.createNestApplication()
    await app.init()
  })

  beforeEach(async () => {
    await truncate()
  })

  describe('Login User', () => {
    test('It Should Authenticate User', async () => {
      const user = await new Factory(User)
        .sequence('role', () => 'superadmin')
        .create({
          name: 'taufik',
          email: 'taufik@mail.com',
          password: 'password',
          phone: '0882398123',
        })

      const res = await supertest(app.getHttpServer())
        .post('/v0/dashboard/user/auth')
        .send({
          email: user.email,
          password: 'password',
        })
        .expect(HttpStatus.OK)

      expect(res.body).toMatchObject({
        userId: user.id,
        name: user.name,
        email: user.email,
        role: user.role,
        token: expect.any(String),
      })
    })

    test('It Should Return Bad Request Error ( Wrong Password )', async () => {
      const user: User = await new Factory(User)
        .sequence('role', () => 'superadmin')
        .create({
          name: 'taufik',
          email: 'taufik@mail.com',
          password: 'password',
          phone: '0882398123',
        })

      const res = await supertest(app.getHttpServer())
        .post('/v0/dashboard/user/auth')
        .send({
          email: user.email,
          password: 'passwords',
        })
        .expect(HttpStatus.BAD_REQUEST)

      expect(res.body).toMatchObject({
        statusCode: 400,
        message: 'Wrong Password',
        error: 'Bad Request',
      })
    })

    test('It Should Return User Not Found Exception', async () => {
      await new Factory(User)
        .sequence('role', () => 'superadmin')
        .create({
          name: 'taufik',
          email: 'taufik@mail.com',
          password: 'password',
          phone: '0882398123',
        })

      const res = await supertest(app.getHttpServer())
        .post('/v0/dashboard/user/auth')
        .send({
          email: 'taufiks@email.com',
          password: 'passwords',
        })
        .expect(HttpStatus.BAD_REQUEST)

      expect(res.body).toMatchObject({
        statusCode: 400,
        error: 'Bad Request',
      })
    })
  })

  describe('Create New User', () => {
    test('It Should Create New Superadmin User', async () => {
      const payload = {
        name: 'taufik',
        email: 'taufik@mail.com',
        password: 'password',
        phone: '0882398123',
        role: 'superadmin',
      }

      const token = login()

      const res = await supertest(app.getHttpServer())
        .post('/v0/dashboard/user')
        .send(payload)
        .set({ Authorization: `Bearer ${token}` })
        .expect(HttpStatus.CREATED)

      expect(res.body).toMatchObject({
        id: expect.any(Number),
        email: payload.email,
        phone: payload.phone,
        role: payload.role,
        createdAt: expect.any(String),
        updatedAt: expect.any(String),
      })
    })
  })
})
