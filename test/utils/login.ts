import { sign } from 'jsonwebtoken'

export const login = (role = 'superadmin') => {
  const token = sign(
    {
      userId: 1,
      name: 'user',
      email: 'user@mail.com',
      role,
    },
    'cepatkaya',
    {
      expiresIn: '7d',
      algorithm: 'HS256',
    },
  )

  return token
}
