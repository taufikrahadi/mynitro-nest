import { getConnection } from 'typeorm'

export const truncate = async () => {
  const entities = getConnection().entityMetadatas
  for (const entity of entities) {
    const repository = getConnection().getRepository(entity.name)
    await repository.query(`
      delete from "${entity.tableName}" where id is not null
    `)
  }
}
