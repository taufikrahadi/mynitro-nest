import { Module } from '@nestjs/common'
import { ConfigModule, ConfigService } from '@nestjs/config'
import { TypeOrmModule } from '@nestjs/typeorm'
import { SnakeNamingStrategy } from 'typeorm-naming-strategies'

@Module({
  imports: [
    ConfigModule.forRoot({
      envFilePath: '.env',
      isGlobal: true,
    }),
    TypeOrmModule.forRootAsync({
      useFactory: (env: ConfigService) => ({
        type: 'postgres',
        host: env.get<string>('DB_HOST'),
        port: env.get<number>('DB_PORT'),
        database: env.get<string>('DB_DATABASE'),
        username: env.get<string>('DB_USERNAME'),
        password: env.get<string>('DB_PASSWORD'),
        synchronize: true,
        cache: true,
        entities: ['dist/domain/**/*{.ts,.js}'],
        namingStrategy: new SnakeNamingStrategy(),
        // logging: env.get<string>('APP_ENV') === 'dev' ? ['query'] : false,
        logging: ['query'],
      }),
      imports: [ConfigModule],
      inject: [ConfigService],
    }),
  ],
})
export class DatabaseModule {}
