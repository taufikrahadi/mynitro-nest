import { MiddlewareConsumer, Module, NestModule } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { DatabaseModule } from '../configs/database.config'
import { V0Module } from './api/v0/v0.module'
import { PaginationMiddleware } from './common/middlewares/pagination.middleware'

@Module({
  imports: [DatabaseModule, V0Module],
  providers: [ConfigService],
})
export class AppModule implements NestModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(PaginationMiddleware).forRoutes('/api/v0/dashboard/order')
  }
}
