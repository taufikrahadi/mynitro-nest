import { ApiProperty, ApiQuery } from '@nestjs/swagger'

export interface IPagination {
  limit: number
  page: number
  skip: number
  take: number
  search?: string
  sort?: string
  orderBy?: string
}

export class PaginationQuery {
  @ApiProperty({
    required: false,
  })
  limit: number

  @ApiProperty({
    required: false,
  })
  page: number

  @ApiProperty({
    required: false,
  })
  sort: 'ASC' | 'DESC'

  @ApiProperty({
    required: false,
  })
  orderBy: string
}
