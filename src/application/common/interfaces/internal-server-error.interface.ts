import { ApiProperty } from '@nestjs/swagger'

export class InternalServerErrorResponse {
  @ApiProperty()
  statusCode: number

  @ApiProperty()
  message: string

  @ApiProperty()
  error: string
}
