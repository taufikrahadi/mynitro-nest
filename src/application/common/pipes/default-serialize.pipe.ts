import { ArgumentMetadata, Injectable, PipeTransform } from '@nestjs/common'

@Injectable()
export class DefaultSerializePipe<T> implements PipeTransform {
  constructor(defaultValue: T, serializeType: any) {
    this.defaultValue = defaultValue
    this.serializeType = serializeType
  }

  private defaultValue: T

  private serializeType: any

  transform(value: any, metadata: ArgumentMetadata) {
    if (!value) value = this.defaultValue
    return this.serializeType(value)
  }
}
