import { createParamDecorator, ExecutionContext } from '@nestjs/common'

export const Pagination = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const context = ctx.switchToHttp().getRequest()
    const pagination = context.pagination

    return data ? pagination?.['data'] : pagination
  },
)
