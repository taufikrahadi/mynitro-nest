import { createParamDecorator, ExecutionContext } from '@nestjs/common'

export const DeviceInfo = createParamDecorator(
  (data: string, ctx: ExecutionContext) => {
    const request = ctx.switchToHttp().getRequest()

    const device = request.device

    return data ? device?.[data] : device
  },
)
