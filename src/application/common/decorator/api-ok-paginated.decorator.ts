import { applyDecorators } from '@nestjs/common'
import { ApiExtraModels, ApiOkResponse, getSchemaPath } from '@nestjs/swagger'
import { PaginationResponse } from 'src/utils/responses/pagination.response'
import { getConnection } from 'typeorm'

export const ApiOkResponsePaginated = (dataDto: any) =>
  applyDecorators(
    ApiExtraModels(PaginationResponse, dataDto),
    ApiOkResponse({
      schema: {
        properties: {
          totalPages: {
            type: 'number',
          },
          currentPage: {
            type: 'number',
          },
          itemsPerPage: {
            type: 'number',
          },
          data: {
            type: 'array',
            items: { $ref: getSchemaPath(dataDto) },
          },
        },
      },
    }),
  )
