import { NestMiddleware } from '@nestjs/common'
import { NextFunction, Request, Response } from 'express'
import { checkPagination } from 'src/utils/helpers/get-pagination'
import { IPagination } from '../interfaces/pagination.interface'

export class PaginationMiddleware implements NestMiddleware {
  use(req: Request, res: Response, next: NextFunction) {
    console.log('anjing')
    const { limit, page, sort, orderBy, search } = {
      limit: Number(req.query?.limit),
      page: Number(req.query?.page),
      sort: req.query.sort ? req.query.sort : 'ASC',
      orderBy: req.query.orderBy,
      search: req.query.search,
    }

    const { skip, take } = checkPagination({ limit, page })

    req['pagination'] = {
      limit,
      page,
      skip,
      take,
      search,
      sort,
      orderBy,
    }

    // next()
  }
}
