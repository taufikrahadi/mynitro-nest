import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common'
import { DeviceService } from 'src/application/api/v0/device/device.service'

@Injectable()
export class DeviceGuard implements CanActivate {
  constructor(private readonly deviceService: DeviceService) {}

  async canActivate(context: ExecutionContext): Promise<boolean> {
    const request = context.switchToHttp().getRequest()

    if (!request.headers.authorization)
      throw new UnauthorizedException('Device Unauthorized')

    const auth = request.headers.authorization.split(' ')[1]

    const [username, password] = Buffer.from(auth, 'base64')
      .toString()
      .split(':')

    const device = await this.deviceService.deviceAuth(username, password)

    console.log(device)

    request['device'] = device

    return true
  }
}
