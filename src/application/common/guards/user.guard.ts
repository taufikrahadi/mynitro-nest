import {
  CanActivate,
  ExecutionContext,
  ForbiddenException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { verify } from 'jsonwebtoken'

@Injectable()
export class UserGuard implements CanActivate {
  constructor(private readonly configService: ConfigService) {}

  canActivate(context: ExecutionContext): boolean {
    try {
      const request = context.switchToHttp().getRequest()
      const { headers } = request

      if (!headers.authorization)
        throw new UnauthorizedException('Request Unauthorized')

      const [type, token] = headers.authorization.split(' ')
      if (type !== 'Bearer' || !token)
        throw new ForbiddenException('Invalid Session')

      const verifyToken = verify(
        token,
        this.configService.get<string>('JWT_SECRET'),
        {
          algorithms: ['HS256'],
        },
      )

      request['user'] = verifyToken
      return true
    } catch (error) {
      throw new ForbiddenException('Invalid Session')
    }
  }
}
