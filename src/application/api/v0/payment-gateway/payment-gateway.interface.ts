export class BaseRequestBody {
  token?: string
  mid?: string
}

export class GetQrRequestBody extends BaseRequestBody {
  tid: string
  amount: number
  reff_no: string
}

export class GetStatusQrRequestBody extends BaseRequestBody {
  tid: string
  trx_id: string
}

export class BaseResponse {
  response?: {
    code: any
    message: string
    latency: any
    host: any
  }
}

export class GetQrResponse extends BaseResponse {
  data: {
    provider: string
    tid: string
    mid: string
    trx_id: string
    reff_no: string
    amount: number
    qr: any
    qr_content: any
  }
}

export class GetStatusQrResponse extends BaseResponse {
  data: {
    tid: string
    mid: string
    trx_id: string
    reff_no: string
    amount: number
    status: string
  }
}

export class GetStatusQrResponseMapper {
  constructor({
    data: { trx_id, mid, tid, amount, reff_no, status },
  }: GetStatusQrResponse) {
    this.trxId = trx_id
    this.mid = mid
    this.tid = tid
    this.amount = Number(amount)
    this.reffNo = reff_no
    this.status = status.toLowerCase()
  }

  tid: string
  mid: string
  trxId: string
  reffNo: string
  amount: number
  status: string
}
