import { HttpService } from '@nestjs/axios'
import { Injectable } from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import {
  GetQrRequestBody,
  GetQrResponse,
  GetStatusQrRequestBody,
  GetStatusQrResponse,
  GetStatusQrResponseMapper,
} from './payment-gateway.interface'

@Injectable()
export class PaymentGatewayService {
  constructor(
    private readonly httpService: HttpService,
    private readonly configService: ConfigService,
  ) {}

  private token: string = this.configService.get<string>('QR_PAYMENT_TOKEN')
  private mid: string = this.configService.get<string>('QR_PAYMENT_MID')

  async getQr({
    amount,
    tid,
    reff_no,
  }: GetQrRequestBody): Promise<GetQrResponse> {
    try {
      const payload: GetQrRequestBody = {
        token: this.token,
        mid: this.mid,
        amount,
        tid,
        reff_no,
      }

      const { data } = await this.httpService
        .post<GetQrResponse>('/get-qr', payload)
        .toPromise()

      return data
    } catch (error) {
      throw error
    }
  }

  async qrStatusPayment(
    payload: GetStatusQrRequestBody,
  ): Promise<GetStatusQrResponseMapper> {
    try {
      payload = {
        ...payload,
        token: this.token,
        mid: this.mid,
      }

      const { data } = await this.httpService
        .post<GetStatusQrResponse>('/status-payment', payload)
        .toPromise()

      console.log(data)

      const result = new GetStatusQrResponseMapper(data)
      return result
    } catch (error) {
      throw error
    }
  }
}
