import { Injectable, UnprocessableEntityException } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Operator } from '../../../../domain/operator/operator.domain'
import {
  checkPagination,
  paginationMetaData,
} from '../../../../utils/helpers/get-pagination'
import { randomString } from '../../../../utils/helpers/random-string'
import { PaginationResponse } from '../../../../utils/responses/pagination.response'
import { Repository } from 'typeorm'
import { CreateOperatorDto, UpdateOperatorDto } from './operator.dto'

@Injectable()
export class OperatorService {
  constructor(
    @InjectRepository(Operator)
    private readonly operatorRepo: Repository<Operator>,
  ) {}

  async createOperator(payload: CreateOperatorDto) {
    try {
      const operator = await this.operatorRepo.save(
        this.operatorRepo.create(payload),
      )

      return operator
    } catch (error) {
      throw error
    }
  }

  async getAllOperator(
    limit: number,
    page: number,
    search?: string,
  ): Promise<PaginationResponse<Operator[]>> {
    try {
      const { skip, take } = checkPagination({ limit, page })
      const query = this.operatorRepo
        .createQueryBuilder('operator')
        .leftJoinAndSelect('operator.device', 'device')
        .skip(skip)
        .take(take)

      let data: [Operator[], number]
      if (search) {
        const queryResult = query
          .where(`operator.fullname ilike %:search%`, { search })
          .orWhere(`operator.phone ilike %:search%`, { search })
          .orWhere(`operator.device.name ilike %:search%`, { search })
          .getManyAndCount()

        data = await queryResult
      } else {
        data = await query.getManyAndCount()
      }

      const response = paginationMetaData(limit, page, data[1], data[0])
      return response
    } catch (error) {
      throw error
    }
  }

  async getOperatorById(id: number): Promise<Operator> {
    try {
      const operator = await this.operatorRepo.findOne(id, {
        relations: ['device'],
        select: [
          'id',
          'fullname',
          'phone',
          'pin',
          'deviceId',
          'device',
          'createdAt',
          'updatedAt',
          'deletedAt',
        ],
      })

      return operator
    } catch (error) {}
  }

  async updateOperator(
    id: string,
    payload: UpdateOperatorDto,
  ): Promise<{ status: boolean }> {
    try {
      await this.operatorRepo.update(id, payload)

      return { status: true }
    } catch (error) {
      throw error
    }
  }

  async regeneratePin(id: number): Promise<{ status: boolean }> {
    try {
      const operator = await this.operatorRepo.findOne(id)
      if (!operator)
        throw new UnprocessableEntityException(
          `operator dengan id ${id} tidak ditemukan`,
        )

      await this.operatorRepo.update(id, {
        pin: randomString('0', 6),
      })

      return {
        status: true,
      }
    } catch (error) {
      throw error
    }
  }

  async deleteOperator(id: number): Promise<{ status: boolean }> {
    try {
      await this.operatorRepo.softDelete(id)

      return {
        status: true,
      }
    } catch (error) {
      throw error
    }
  }
}
