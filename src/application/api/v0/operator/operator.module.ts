import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { Operator } from '../../../../domain/operator/operator.domain'
import { OperatorController } from './operator.controller'
import { OperatorService } from './operator.service'

@Module({
  imports: [TypeOrmModule.forFeature([Operator])],
  providers: [OperatorService],
  controllers: [OperatorController],
})
export class OperatorModule {}
