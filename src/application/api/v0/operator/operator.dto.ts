import {
  IsInt,
  IsNotEmpty,
  IsNumberString,
  IsOptional,
  IsString,
  IsUUID,
  MinLength,
} from 'class-validator'
import { Operator } from '../../../../domain/operator/operator.domain'
import { IsExists } from '../../../../utils/decorators/is-exists.decorator'
import { IsUnique } from '../../../../utils/decorators/is-unique.decorator'

export class CreateOperatorDto {
  @IsString()
  @IsNotEmpty()
  fullname: string

  @IsNumberString()
  @MinLength(10)
  phone: string

  @IsNotEmpty()
  @IsInt()
  deviceId: number
}

export class UpdateOperatorDto {
  @IsUUID()
  @IsExists('id', Operator)
  @IsNotEmpty()
  id: string

  @IsString()
  @IsOptional()
  fullname: string

  @IsUnique('phone', Operator, { message: 'No hp sudah digunakan' }, 'id')
  @IsOptional()
  @IsNumberString()
  phone: string

  @IsOptional()
  deviceId: number
}
