import {
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Query,
} from '@nestjs/common'
import { DefaultSerializePipe } from '../../../common/pipes/default-serialize.pipe'
import { Operator } from '../../../../domain/operator/operator.domain'
import { CreateOperatorDto, UpdateOperatorDto } from './operator.dto'
import { OperatorService } from './operator.service'
import { ApiTags } from '@nestjs/swagger'

@Controller('v0')
@ApiTags('dashboard')
export class OperatorController {
  constructor(private readonly operatorService: OperatorService) {}

  @Post('/dashboard/operator')
  createOperator(@Body() payload: CreateOperatorDto): Promise<Operator> {
    return this.operatorService.createOperator(payload)
  }

  @Get('/dashboard/operator')
  getAllOperator(
    @Query('limit', new DefaultSerializePipe<number>(10, Number)) limit: number,
    @Query('page', new DefaultSerializePipe(1, Number)) page: number,
    @Query('search', new DefaultValuePipe('')) search?: string,
  ) {
    return this.operatorService.getAllOperator(limit, page, search)
  }

  @Get('/dashboard/operator/:id')
  getOperatorById(@Param('id') id: number) {
    return this.operatorService.getOperatorById(id)
  }

  @Put('/dashboard/operator/:id')
  updateOperator(@Param('id') id: string, @Body() payload: UpdateOperatorDto) {
    return this.operatorService.updateOperator(id, payload)
  }

  @Delete('/dashboard/operator/:id')
  deleteOperator(@Param('id') id: number) {
    return this.operatorService.deleteOperator(id)
  }
}
