import { Type } from 'class-transformer'
import {
  IsNotEmpty,
  IsOptional,
  IsString,
  Matches,
  MinLength,
  IsNumber,
  IsDefined,
  IsNotEmptyObject,
  IsUUID,
} from 'class-validator'
import { IsExists } from '../../../../utils/decorators/is-exists.decorator'
import { Device, PriceSet } from '../../../../domain/device/device.domain'
import { IsUnique } from '../../../../utils/decorators/is-unique.decorator'
import { ApiProperty } from '@nestjs/swagger'

export class RegisterDeviceDto {
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  @ApiProperty()
  imei: string
}

export class CreateDeviceDto {
  @IsString()
  @IsNotEmpty()
  @MinLength(8)
  @IsUnique('imei', Device, {
    message: ({ value }) => `Device Dengan IMEI '${value}' Sudah Ada`,
  })
  @ApiProperty()
  // @Matches(/^[a-zA-Z0-9 !@#$%^&*)(]{2,20}$/, {
  //   message: 'Format IMEI Tidak Valid',
  // })
  imei: string

  @IsString()
  @IsNotEmpty()
  @MinLength(3)
  @ApiProperty()
  name: string

  @IsString()
  @IsOptional()
  @ApiProperty()
  address: string

  @IsNumber()
  @IsOptional()
  @ApiProperty()
  longitude: number

  @IsNumber()
  @IsOptional()
  @ApiProperty()
  latitude: number

  @IsString()
  @IsNotEmpty()
  @MinLength(3)
  @ApiProperty()
  qrPaymentTid: string

  @Type(() => PriceSet)
  @IsNotEmpty()
  @IsDefined()
  @IsNotEmptyObject()
  @ApiProperty({
    type: PriceSet,
  })
  priceSet: PriceSet
}

export class UpdateDeviceDto {
  @IsUUID()
  @IsNotEmpty()
  @IsExists('id', Device, {
    message: ({ value }) => `Device with id '${value}' not found`,
  })
  id: string

  @IsString()
  @IsOptional()
  @MinLength(8)
  @IsUnique(
    'imei',
    Device,
    {
      message: ({ value }) => `Device Dengan IMEI '${value}' Sudah Ada`,
    },
    'id',
  )
  @Matches(/^[a-zA-Z0-9 !@#$%^&*)(]{2,20}$/, {
    message: 'Format IMEI Tidak Valid',
  })
  imei: string

  @IsString()
  @IsOptional()
  @MinLength(3)
  name: string

  @IsString()
  @IsOptional()
  address: string

  @IsNumber()
  @IsOptional()
  longitude: number

  @IsNumber()
  @IsOptional()
  latitude: number

  @IsString()
  @IsOptional()
  @MinLength(3)
  qrPaymentTid: string

  @Type(() => PriceSet)
  @IsOptional()
  @IsDefined()
  @IsNotEmptyObject()
  priceSet: PriceSet
}
