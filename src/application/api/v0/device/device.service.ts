import {
  ForbiddenException,
  Injectable,
  UnauthorizedException,
  UnprocessableEntityException,
} from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Device } from '../../../../domain/device/device.domain'
import {
  checkPagination,
  paginationMetaData,
} from '../../../../utils/helpers/get-pagination'
import { randomString } from '../../../../utils/helpers/random-string'
import { PaginationResponse } from '../../../../utils/responses/pagination.response'
import { ILike, IsNull, Not, Repository } from 'typeorm'
import { UpdateDeviceDto } from './device.dto'
import { compareSync, genSaltSync, hashSync } from 'bcrypt'
import { toggleStatusPerDevice } from 'src/application/event/emergency-payment.event'

@Injectable()
export class DeviceService {
  constructor(
    @InjectRepository(Device) private readonly deviceRepo: Repository<Device>,
  ) {}

  async createDevice(payload: Device) {
    try {
      const secretKey = randomString('aA0', 10)

      const device = await this.deviceRepo.save(
        this.deviceRepo.create({
          ...payload,
          secretKey,
        }),
      )

      return {
        ...device,
        priceSet: JSON.parse(String(device.priceSet)),
        rawSecretKey: secretKey,
      }
    } catch (error) {
      throw error
    }
  }

  async findDeviceByImei(imei: string, withSecretKey = false): Promise<Device> {
    try {
      const selectField: (keyof Device)[] = [
        'id',
        'imei',
        'name',
        'qrPaymentTid',
        'priceSet',
        'latitude',
        'longitude',
        'createdAt',
        'updatedAt',
      ]

      console.log(withSecretKey)

      const device = await this.deviceRepo.findOne({
        where: {
          imei,
        },
        select: withSecretKey ? [...selectField, 'secretKey'] : selectField,
      })

      return device
    } catch (error) {
      throw error
    }
  }

  async deviceAuth(imei: string, secretKey: string): Promise<Device> {
    try {
      const device = await this.findDeviceByImei(imei, true)
      if (!device) throw new ForbiddenException(`Device Unauthorized`)

      const comparePassword = compareSync(secretKey, device.secretKey)

      if (!comparePassword) throw new UnauthorizedException(`Wrong Password`)

      return device
    } catch (error) {
      throw error
    }
  }

  async getDeviceById(id: string) {
    try {
      const device = await this.deviceRepo.findOne(id)

      return device
    } catch (error) {
      throw error
    }
  }

  async getAllDevice(
    limit: number,
    page: number,
    search?: string,
  ): Promise<PaginationResponse<Device[]>> {
    try {
      const where = search
        ? [{ name: ILike(`%${search}%`) }, { imei: ILike(`%${search}%`) }]
        : {}

      const { skip, take } = checkPagination({ limit, page })
      const [devices, total] = await this.deviceRepo.findAndCount({
        where,
        skip,
        take,
        cache: 10000,
      })

      const result: PaginationResponse<Device[]> = paginationMetaData<Device[]>(
        limit,
        page,
        total,
        devices.map((device) => ({
          ...device,
          priceSet: JSON.parse(String(device.priceSet)),
        })),
      )

      return result
    } catch (error) {
      throw error
    }
  }

  async updateDevice(id: string, payload: UpdateDeviceDto) {
    try {
      await this.deviceRepo.update(id, payload)

      return await this.getDeviceById(id)
    } catch (error) {
      throw error
    }
  }

  async deleteDevice(id: number) {
    try {
      await this.deviceRepo.softDelete(id)

      return { status: true }
    } catch (error) {
      throw error
    }
  }

  async getGlobalEmergencyPaymentStatus() {
    try {
      const data = await this.deviceRepo.query(`
        select cast(count(d."emergency_payment_status") as int) as count_status, d."emergency_payment_status" from "device" d
        group by d."emergency_payment_status"
        order by count_status desc 
      `)

      const [status] = data

      return {
        status: status.emergency_payment_status,
      }
    } catch (error) {
      throw error
    }
  }

  async toggleGlobalEmergencyPaymentStatus() {
    try {
      const { status } = await this.getGlobalEmergencyPaymentStatus()

      const updateDevice = await this.deviceRepo.update(
        {
          id: Not(IsNull()),
        },
        {
          emergencyPaymentStatus: !status,
        },
      )

      toggleStatusPerDevice.emit('status-change', {
        status: !status,
      })

      return updateDevice
    } catch (error) {}
  }

  async registerDevice(deviceImei: string) {
    try {
      const secretKey = randomString('aA0', 10)

      const findDevice = await this.deviceRepo.findOne({
        where: {
          imei: deviceImei,
        },
        withDeleted: true,
      })

      if (!findDevice) {
        await this.deviceRepo.save(
          this.deviceRepo.create({
            imei: deviceImei,
            secretKey,
          }),
        )

        return {
          rawSecretKey: secretKey,
        }
      }

      if (findDevice.deletedAt)
        throw new UnprocessableEntityException('Device Has Been Deleted')

      await this.deviceRepo.update(findDevice.id, {
        secretKey: hashSync(secretKey, genSaltSync(12)),
      })

      return {
        rawSecretKey: secretKey,
      }
    } catch (error) {
      throw error
    }
  }
}
