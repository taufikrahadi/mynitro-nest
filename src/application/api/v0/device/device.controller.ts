import {
  Body,
  Controller,
  DefaultValuePipe,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Put,
  Query,
  UseGuards,
} from '@nestjs/common'
import { DefaultSerializePipe } from '../../../common/pipes/default-serialize.pipe'
import { Roles } from '../../../../utils/decorators/roles.decorator'
import { RolesGuard } from '../../../common/guards/roles.guard'
import { UserGuard } from '../../../common/guards/user.guard'
import {
  CreateDeviceDto,
  RegisterDeviceDto,
  UpdateDeviceDto,
} from './device.dto'
import { DeviceService } from './device.service'
import { DeviceGuard } from 'src/application/common/guards/device.guard'
import { Device as DeviceDecorator } from 'src/utils/decorators/device.decorator'
import { Device } from 'src/domain/device/device.domain'
import {
  ApiBasicAuth,
  ApiBearerAuth,
  ApiOkResponse,
  ApiTags,
} from '@nestjs/swagger'

@Controller('v0')
export class DeviceController {
  constructor(private readonly deviceService: DeviceService) {}

  @Post('/device/register')
  @ApiTags('device')
  async registerDevice(@Body() { imei }: RegisterDeviceDto) {
    return await this.deviceService.registerDevice(imei)
  }

  @Get('/device/info')
  @UseGuards(DeviceGuard)
  @ApiBasicAuth()
  @ApiOkResponse({
    type: Device,
  })
  @ApiTags('device')
  async getDeviceInfo(@DeviceDecorator() { imei }: Device) {
    return this.deviceService.findDeviceByImei(imei)
  }

  @Post('/dashboard/device')
  @UseGuards(UserGuard, RolesGuard)
  @Roles('superadmin')
  @ApiBearerAuth()
  @ApiTags('dashboard')
  async createNewDevice(@Body() payload: CreateDeviceDto) {
    return await this.deviceService.createDevice(payload)
  }

  @Get('/dashboard/device/:id')
  @UseGuards(UserGuard, RolesGuard)
  @Roles('superadmin')
  @ApiTags('dashboard')
  @ApiBearerAuth()
  async getDeviceById(@Param('id') id: string) {
    return await this.deviceService.getDeviceById(id)
  }

  @Get('/dashboard/device')
  @UseGuards(UserGuard, RolesGuard)
  @Roles('superadmin')
  @ApiTags('dashboard')
  @ApiBearerAuth()
  async getAllDevices(
    @Query('limit', new DefaultSerializePipe(10, Number)) limit: number,
    @Query('page', new DefaultSerializePipe(1, Number)) page: number,
    @Query('search', new DefaultValuePipe('')) search?: string,
  ) {
    return await this.deviceService.getAllDevice(limit, page, search)
  }

  @Put('/dashboard/device/:id')
  @UseGuards(UserGuard, RolesGuard)
  @Roles('superadmin')
  @ApiBearerAuth()
  @ApiTags('dashboard')
  async updateDevice(
    @Body() payload: UpdateDeviceDto,
    @Param('id') id: string,
  ) {
    return await this.deviceService.updateDevice(id, payload)
  }

  @Delete('/dashboard/device/:id')
  @UseGuards(UserGuard, RolesGuard)
  @Roles('superadmin')
  @ApiBearerAuth()
  @ApiTags('dashboard')
  async deleteDevice(@Param('id') id: number) {
    return await this.deviceService.deleteDevice(id)
  }

  @Get('/dashboard/device/emergency-payment')
  @ApiTags('dashboard')
  @UseGuards(UserGuard, RolesGuard)
  @Roles('superadmin')
  @ApiBearerAuth()
  async getGlobalEmergencyPaymentStatus() {
    return this.deviceService.getGlobalEmergencyPaymentStatus()
  }

  @Patch('/dashboard/device/emergency-payment')
  @ApiTags('dashboard')
  @UseGuards(UserGuard, RolesGuard)
  @Roles('superadmin')
  @ApiBearerAuth()
  async toggleGlobalEmergencyPaymentStatus() {
    return this.deviceService.toggleGlobalEmergencyPaymentStatus()
  }
}
