import { Module } from '@nestjs/common'
import { DeviceModule } from './device/device.module'
import { OperatorModule } from './operator/operator.module'
import { OrderModule } from './order/order.module'
import { PaymentModule } from './payment/payment.module'
import { UserModule } from './user/user.module'

@Module({
  imports: [
    OperatorModule,
    DeviceModule,
    UserModule,
    OrderModule,
    PaymentModule,
  ],
})
export class V0Module {}
