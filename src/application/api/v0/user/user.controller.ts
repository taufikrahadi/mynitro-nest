import {
  Body,
  Controller,
  DefaultValuePipe,
  Get,
  HttpCode,
  Param,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common'
import { DefaultSerializePipe } from '../../../common/pipes/default-serialize.pipe'
import { Roles } from '../../../../utils/decorators/roles.decorator'
import { RolesGuard } from '../../../common/guards/roles.guard'
import { UserGuard } from '../../../common/guards/user.guard'
import { CreateUserDto, UserAuthDto } from './user.dto'
import { UserService } from './user.service'
import { ApiTags } from '@nestjs/swagger'

@Controller('v0')
@ApiTags('dashboard')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @Post('/dashboard/user')
  // @UseGuards(UserGuard, RolesGuard)
  // @Roles('superadmin')
  createUser(@Body() payload: CreateUserDto) {
    return this.userService.createUser(payload)
  }

  @Post('/dashboard/user/auth')
  @HttpCode(200)
  userAuth(@Body() payload: UserAuthDto) {
    return this.userService.userAuth(payload)
  }

  @Get('/dashboard/user')
  @UseGuards(UserGuard, RolesGuard)
  @Roles('superadmin')
  getAllUser(
    @Query('limit', new DefaultSerializePipe(10, Number)) limit: number,
    @Query('page', new DefaultSerializePipe(1, Number)) page: number,
    @Query('search', new DefaultValuePipe('')) search: string,
  ) {
    return this.userService.getAllUser({
      limit,
      page,
      search,
    })
  }

  @Get('/dashboard/user/:id')
  @UseGuards(UserGuard, RolesGuard)
  @Roles('superadmin')
  getUserById(@Param('id') id: number) {
    return this.userService.getUserById(id)
  }
}
