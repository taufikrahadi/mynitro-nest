import {
  IsArray,
  IsEmail,
  IsIn,
  IsNotEmpty,
  IsOptional,
  IsString,
  MinLength,
} from 'class-validator'
import { User } from '../../../../domain/user/user.domain'
import { IsExists } from '../../../../utils/decorators/is-exists.decorator'
import { UserToken } from '../../../../domain/user/user.interface'
import { IsUnique } from '../../../../utils/decorators/is-unique.decorator'
import { ApiProperty } from '@nestjs/swagger'

export class CreateUserDto {
  @IsEmail()
  @IsNotEmpty()
  @IsUnique('email', User, {
    message: ({ value }) => `User with email '${value}' already exists`,
  })
  email: string

  @IsString()
  @IsNotEmpty()
  @MinLength(3)
  name: string

  @IsString()
  @IsNotEmpty()
  @MinLength(10)
  phone: string

  @IsString()
  @IsNotEmpty()
  @MinLength(6)
  password: string

  @IsIn(['superadmin', 'admin'])
  @IsNotEmpty()
  role: 'superadmin' | 'admin'

  @IsArray()
  @IsOptional()
  devices: Array<any>
}

export class UserAuthDto {
  @IsEmail()
  @IsNotEmpty()
  @IsExists('email', User, {
    message: ({ value }) => {
      return `User with email '${value}' not found`
    },
  })
  @ApiProperty()
  email: string

  @IsString()
  @IsNotEmpty()
  @ApiProperty()
  password: string
}

export class UserAuthResponse extends UserToken {
  token: string
}
