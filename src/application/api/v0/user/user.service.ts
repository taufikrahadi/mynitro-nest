import { BadRequestException, Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { User } from '../../../../domain/user/user.domain'
import { ILike, Repository } from 'typeorm'
import { CreateUserDto, UserAuthDto, UserAuthResponse } from './user.dto'
import { compareSync } from 'bcrypt'
import { sign } from 'jsonwebtoken'
import { ConfigService } from '@nestjs/config'
import { UserToken } from '../../../../domain/user/user.interface'
import { PaginationResponse } from '../../../../utils/responses/pagination.response'
import {
  checkPagination,
  paginationMetaData,
} from '../../../../utils/helpers/get-pagination'

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(User) private readonly userRepo: Repository<User>,
    private readonly configService: ConfigService,
  ) {}

  async getUserById(id: number) {
    return await this.userRepo.findOne({
      where: {
        id,
      },
    })
  }

  async getAllUser({
    limit,
    page,
    search,
  }: {
    limit: number
    page: number
    search?: string
  }): Promise<PaginationResponse<User[]>> {
    const where = search
      ? [{ name: ILike(`%${search}%`) }, { email: ILike(`%${search}%`) }]
      : {}

    const { skip, take } = checkPagination({ limit, page })
    const [users, total] = await this.userRepo.findAndCount({
      where,
      skip,
      take,
      cache: 10000,
    })

    const result: PaginationResponse<User[]> = paginationMetaData<User[]>(
      limit,
      page,
      total,
      users,
    )

    return result
  }

  async createUser(payload: CreateUserDto): Promise<User> {
    const deviceId = payload.devices
      ? payload.devices.map((device) => ({
          deviceId: device,
        }))
      : undefined
    const user = await this.userRepo.save(
      this.userRepo.create({
        ...payload,
        devices: deviceId,
      }),
    )

    delete user.password
    return user
  }

  async userAuth({ email, password }: UserAuthDto): Promise<UserAuthResponse> {
    try {
      const user = await this.userRepo.findOne({
        where: {
          email,
        },
        select: ['id', 'name', 'email', 'role', 'password'],
      })

      const comparePassword = compareSync(password, user.password)
      if (!comparePassword) throw new BadRequestException(`Wrong Password`)

      const tokenPayload: UserToken = {
        userId: user.id,
        name: user.name,
        email: user.email,
        role: user.role,
      }
      const token = sign(
        tokenPayload,
        this.configService.get<string>('JWT_SECRET'),
        {
          expiresIn: '7d',
          algorithm: 'HS256',
        },
      )

      return {
        ...tokenPayload,
        token,
      }
    } catch (error) {
      throw error
    }
  }
}
