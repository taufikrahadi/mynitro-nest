import { Module } from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { QrPayment } from 'src/domain/qr-payment/qr-payment.domain'
import { QrPaymentService } from './qr-payment.service'

@Module({
  imports: [TypeOrmModule.forFeature([QrPayment])],
  providers: [QrPaymentService],
  exports: [QrPaymentService, TypeOrmModule],
})
export class QrPaymentModule {}
