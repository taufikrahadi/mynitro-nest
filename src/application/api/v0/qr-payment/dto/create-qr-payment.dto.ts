export class CreateQrPaymentDto {
  paymentId: string
  host: string
  tid: string
  mid: string
  reffNo: string
  trxId: string
  provider: string
  amount: number
  datetime: Date
  status: string
}
