import { InjectRepository } from '@nestjs/typeorm'
import { QrPayment } from 'src/domain/qr-payment/qr-payment.domain'
import { Repository } from 'typeorm'
import { CreateQrPaymentDto } from './dto/create-qr-payment.dto'

export class QrPaymentService {
  constructor(
    @InjectRepository(QrPayment)
    private readonly qrPaymentRepo: Repository<QrPayment>,
  ) {}

  async create(payload: CreateQrPaymentDto, transaction?: boolean) {
    try {
      return await this.qrPaymentRepo.save(this.qrPaymentRepo.create(payload), {
        transaction,
      })
    } catch (error) {
      throw error
    }
  }
}
