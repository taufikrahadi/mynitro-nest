import { ApiProperty } from '@nestjs/swagger'

export class CreateOrderDto {
  @ApiProperty()
  vehicleType: string
  @ApiProperty()
  orderType: string
  @ApiProperty()
  tireCount: number
  @ApiProperty()
  price: number
  @ApiProperty()
  provider: string
}
