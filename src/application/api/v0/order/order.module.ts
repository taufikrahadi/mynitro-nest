import {
  MiddlewareConsumer,
  Module,
  NestModule,
  RequestMethod,
} from '@nestjs/common'
import { TypeOrmModule } from '@nestjs/typeorm'
import { PaginationMiddleware } from 'src/application/common/middlewares/pagination.middleware'
import { Order } from 'src/domain/order/order.domain'
import { DeviceModule } from '../device/device.module'
import { PaymentGatewayModule } from '../payment-gateway/payment-gateway.module'
import { PaymentModule } from '../payment/payment.module'
import { QrPaymentModule } from '../qr-payment/qr-payment.module'
import { OrderController } from './order.controller'
import { OrderService } from './order.service'

@Module({
  imports: [
    TypeOrmModule.forFeature([Order]),
    PaymentModule,
    QrPaymentModule,
    PaymentGatewayModule,
    DeviceModule,
  ],
  providers: [OrderService],
  controllers: [OrderController],
  exports: [OrderService],
})
export class OrderModule {}
