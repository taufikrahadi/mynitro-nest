import {
  Body,
  Controller,
  DefaultValuePipe,
  Get,
  HttpStatus,
  InternalServerErrorException,
  Post,
  Query,
  UseGuards,
} from '@nestjs/common'
import {
  ApiBasicAuth,
  ApiBearerAuth,
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger'
import { ApiOkResponsePaginated } from 'src/application/common/decorator/api-ok-paginated.decorator'
import { DeviceInfo } from 'src/application/common/decorator/device.decorator'
import { DeviceGuard } from 'src/application/common/guards/device.guard'
import { InternalServerErrorResponse } from 'src/application/common/interfaces/internal-server-error.interface'
import { DefaultSerializePipe } from 'src/application/common/pipes/default-serialize.pipe'
import { Order } from 'src/domain/order/order.domain'
import { PaginationResponse } from 'src/utils/responses/pagination.response'
import { CreateOrderDto } from './dto/create-order.dto'
import { OrderService } from './order.service'

@Controller()
export class OrderController {
  constructor(private readonly orderService: OrderService) {}

  @Get('/v0/dashboard/order')
  @ApiTags('dashboard')
  @ApiBearerAuth()
  @ApiInternalServerErrorResponse({
    type: InternalServerErrorResponse,
  })
  @ApiQuery({
    name: 'limit',
    required: false,
  })
  @ApiQuery({
    name: 'page',
    required: false,
  })
  @ApiQuery({
    name: 'sort',
    required: false,
  })
  @ApiQuery({
    name: 'orderBy',
    required: false,
  })
  @ApiQuery({
    name: 'search',
    required: false,
  })
  @ApiOkResponsePaginated(Order)
  getAllOrder(
    @Query('limit', new DefaultSerializePipe(10, Number)) limit: number,
    @Query('page', new DefaultSerializePipe(1, Number)) page: number,
    @Query('sort', new DefaultSerializePipe('DESC', String)) sort?: string,
    @Query('orderBy') order?: string,
    @Query('search', new DefaultValuePipe('')) search?: string,
  ) {
    throw new InternalServerErrorException('anjing')
    return this.orderService.getAllOrders(limit, page, sort, order, search)
  }

  @Post('/v0/device/order/cashless')
  @UseGuards(DeviceGuard)
  @ApiTags('device')
  @ApiBasicAuth()
  createOrderCashless(@Body() payload: CreateOrderDto, @DeviceInfo() device) {
    return this.orderService.createOrderCashlessPayment(payload, device)
  }

  @Post('/v0/device/order/cash')
  @UseGuards(DeviceGuard)
  @ApiTags('device')
  @ApiBasicAuth()
  createOrderCash(@Body() payload: CreateOrderDto, @DeviceInfo() device) {
    return this.orderService.createOrderCashPayment(payload, device)
  }
}
