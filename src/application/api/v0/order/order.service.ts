import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Order } from 'src/domain/order/order.domain'
import { Repository, getConnection, ILike } from 'typeorm'
import { PaymentService } from '../payment/payment.service'
import { QrPaymentService } from '../qr-payment/qr-payment.service'
import { CreateOrderDto } from './dto/create-order.dto'
import { PaymentGatewayService } from '../payment-gateway/payment-gateway.service'
import { ConfigService } from '@nestjs/config'
import * as moment from 'moment-timezone'
import { Device } from 'src/domain/device/device.domain'
import { PaginationResponse } from 'src/utils/responses/pagination.response'
import {
  checkPagination,
  paginationMetaData,
} from 'src/utils/helpers/get-pagination'

@Injectable()
export class OrderService {
  constructor(
    @InjectRepository(Order) private readonly orderRepo: Repository<Order>,
    private readonly paymentService: PaymentService,
    private readonly qrPaymentService: QrPaymentService,
    private readonly paymentGateway: PaymentGatewayService,
    private readonly configService: ConfigService,
  ) {}

  async getAllOrders(
    limit: number,
    page: number,
    sort: string = 'DESC',
    orderBy: string = 'createdAt',
    search?: string,
  ): Promise<PaginationResponse<Order[]>> {
    try {
      const where = search
        ? [{ name: ILike(`%${search}%`) }, { imei: ILike(`%${search}%`) }]
        : {}
      const { skip, take } = checkPagination({ limit, page })

      const [orders, count] = await this.orderRepo.findAndCount({
        skip,
        take,
        where,
        order: {
          [orderBy]: sort,
        },
      })

      const result: PaginationResponse<Order[]> = paginationMetaData(
        limit,
        page,
        count,
        orders,
      )

      return result
    } catch (error) {
      throw error
    }
  }

  /**
   *
   * @param payload order payload
   * @param device device data
   * @returns order data with qr code data
   */
  async createOrderCashlessPayment(payload: CreateOrderDto, device: Device) {
    const queryRunner = getConnection().createQueryRunner()
    await queryRunner.startTransaction()
    try {
      const order = await this.orderRepo.save(
        this.orderRepo.create({ ...payload, deviceId: device.id }),
        {
          transaction: true,
        },
      )

      const payment = await this.paymentService.createPayment(
        {
          amount: payload.price,
          orderId: order.id,
          paymentType: 'cashless',
          provider: 'shopee',
          status: 'waiting',
        },
        true,
      )

      const reff_no = `${this.configService.get<string>(
        'QR_PAYMENT_REFF_NO_PREFIX',
      )}${moment.now()}${device.qrPaymentTid}`
      const { data, response } = await this.paymentGateway.getQr({
        amount: payload.price,
        reff_no,
        tid: device.qrPaymentTid,
      })

      const qrPayment = await this.qrPaymentService.create({
        amount: payload.price,
        datetime: new Date(moment.now()),
        host: response.host,
        mid: data.mid,
        paymentId: payment.id,
        provider: payload.provider,
        reffNo: reff_no,
        status: payment.status,
        tid: device.qrPaymentTid,
        trxId: data.trx_id,
      })

      await queryRunner.commitTransaction()
      return {
        order,
        qr: data,
      }
    } catch (error) {
      await queryRunner.rollbackTransaction()
      throw error
    }
  }

  /**
   *
   * @param payload order payload
   * @param device device data
   * @returns order data with qr code data
   */
  async createOrderCashPayment(payload: CreateOrderDto, device: Device) {
    const queryRunner = getConnection().createQueryRunner()
    await queryRunner.startTransaction()
    try {
      const order = await this.orderRepo.save(
        this.orderRepo.create({
          ...payload,
          deviceId: device.id,
          status: 'success',
        }),
        {
          transaction: true,
        },
      )

      const payment = await this.paymentService.createPayment(
        {
          amount: payload.price,
          orderId: order.id,
          paymentType: 'cash',
          provider: 'cash',
          status: 'success',
        },
        true,
      )

      await queryRunner.commitTransaction()
      return {
        order,
      }
    } catch (error) {
      await queryRunner.rollbackTransaction()
      throw error
    }
  }
}
