import { Injectable } from '@nestjs/common'
import { InjectRepository } from '@nestjs/typeorm'
import { Payment } from 'src/domain/payment/payment.domain'
import {
  checkPagination,
  paginationMetaData,
} from 'src/utils/helpers/get-pagination'
import { PaginationResponse } from 'src/utils/responses/pagination.response'
import { ILike, Repository } from 'typeorm'
import { CreatePaymentDto } from './dto/create-payment.dto'

@Injectable()
export class PaymentService {
  constructor(
    @InjectRepository(Payment)
    private readonly paymentRepo: Repository<Payment>,
  ) {}

  async getAllPayments(
    type: 'cash' | 'cashless',
    limit: number,
    page: number,
    sort: string = 'DESC',
    orderBy: string = 'createdAt',
    search?: string,
  ): Promise<PaginationResponse<Payment[]>> {
    try {
      const where = search
        ? [{ name: ILike(`%${search}%`) }, { imei: ILike(`%${search}%`) }]
        : {}
      const { skip, take } = checkPagination({ limit, page })

      const [orders, count] = await this.paymentRepo.findAndCount({
        skip,
        take,
        where,
        order: {
          [orderBy]: sort,
        },
      })

      const result: PaginationResponse<Payment[]> = paginationMetaData(
        limit,
        page,
        count,
        orders,
      )

      return result
    } catch (error) {
      throw error
    }
  }

  async createPayment(payload: CreatePaymentDto, transaction?: boolean) {
    try {
      return await this.paymentRepo.save(this.paymentRepo.create(payload), {
        transaction,
      })
    } catch (error) {
      throw error
    }
  }
}
