export class CreatePaymentDto {
  orderId: string
  amount: number
  paymentType: 'cashless' | 'cash'
  provider: string
  status: string
}
