import { Controller, DefaultValuePipe, Get, Param, Query } from '@nestjs/common'
import {
  ApiInternalServerErrorResponse,
  ApiOkResponse,
  ApiParam,
  ApiQuery,
  ApiTags,
} from '@nestjs/swagger'
import { ApiOkResponsePaginated } from 'src/application/common/decorator/api-ok-paginated.decorator'
import { InternalServerErrorResponse } from 'src/application/common/interfaces/internal-server-error.interface'
import { DefaultSerializePipe } from 'src/application/common/pipes/default-serialize.pipe'
import { Payment } from 'src/domain/payment/payment.domain'
import { PaginationResponse } from 'src/utils/responses/pagination.response'
import { PaymentService } from './payment.service'

@Controller()
export class PaymentController {
  constructor(private readonly paymentService: PaymentService) {}

  @Get('/v0/dashboard/payment/:type')
  @ApiParam({
    name: 'type',
    description: 'select payment type cash or cashless',
    enum: ['cash', 'cashless'],
  })
  @ApiTags('dashboard')
  @ApiQuery({
    name: 'limit',
    required: false,
  })
  @ApiQuery({
    name: 'page',
    required: false,
  })
  @ApiQuery({
    name: 'sort',
    required: false,
  })
  @ApiQuery({
    name: 'orderBy',
    required: false,
  })
  @ApiQuery({
    name: 'search',
    required: false,
  })
  @ApiOkResponsePaginated(Payment)
  @ApiInternalServerErrorResponse({
    type: InternalServerErrorResponse,
  })
  async getAllPayment(
    @Param('type') type: 'cash' | 'cashless',
    @Query('limit', new DefaultSerializePipe(10, Number)) limit: number,
    @Query('page', new DefaultSerializePipe(1, Number)) page: number,
    @Query('sort', new DefaultSerializePipe('DESC', String)) sort?: string,
    @Query('orderBy') order?: string,
    @Query('search', new DefaultValuePipe('')) search?: string,
  ) {
    return this.paymentService.getAllPayments(
      type,
      limit,
      page,
      sort,
      order,
      search,
    )
  }
}
