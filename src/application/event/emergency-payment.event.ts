import { EventEmitter } from 'events'

export const toggleStatusPerDevice = new EventEmitter({
  captureRejections: true,
})
