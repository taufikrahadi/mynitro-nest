import { EventEmitter } from 'events'
import { Order } from 'src/domain/order/order.domain'
import { Payment } from 'src/domain/payment/payment.domain'
import { QrPayment } from 'src/domain/qr-payment/qr-payment.domain'
import { getRepository } from 'typeorm'

export const qrPaymentEvent = new EventEmitter({
  captureRejections: true,
})

qrPaymentEvent.on('success', async ({ qrPaymentId }) => {
  const qrPayment = await getRepository(QrPayment)
    .createQueryBuilder('qr')
    .innerJoinAndSelect('qr.payment', 'payment')
    .where('qr.id = :id', { id: qrPaymentId })
    .getOne()

  const update = [
    await getRepository(QrPayment)
      .createQueryBuilder('qr')
      .update()
      .set({
        status: 'success',
      })
      .where('id = :id', { id: qrPaymentId })
      .execute(),

    await getRepository(Payment)
      .createQueryBuilder('payment')
      .update()
      .set({
        status: 'success',
      })
      .where('id = :id', { id: qrPayment.payment.id })
      .execute(),

    await getRepository(Order)
      .createQueryBuilder('order')
      .update()
      .set({
        status: 'success',
      })
      .where('id = :id', { id: qrPayment.payment.orderId })
      .execute(),
  ]

  await Promise.all(update)
  console.log('transaction success')
})
