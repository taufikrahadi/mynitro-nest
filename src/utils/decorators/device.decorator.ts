import { createParamDecorator, ExecutionContext } from '@nestjs/common'

export const Device = createParamDecorator(
  (data: unknown, context: ExecutionContext) => {
    const request = context.switchToHttp().getRequest()

    return request.device
  },
)
