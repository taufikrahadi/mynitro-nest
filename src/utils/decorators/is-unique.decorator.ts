import {
  registerDecorator,
  ValidationArguments,
  ValidationOptions,
  ValidatorConstraint,
  ValidatorConstraintInterface,
} from 'class-validator'
import { getRepository } from 'typeorm'

export function IsUnique(
  key: string,
  repository: any,
  validationOptions?: ValidationOptions,
  compareField?: string,
) {
  return (object: any, propertyName: string) => {
    registerDecorator({
      target: object.constructor,
      propertyName,
      options: validationOptions,
      constraints: [key, repository, compareField],
      validator: IsUniqueConstraints,
    })
  }
}

@ValidatorConstraint({ name: 'IsUnique' })
export class IsUniqueConstraints implements ValidatorConstraintInterface {
  async validate(value: any, args: ValidationArguments) {
    const [key, repo, compareField] = args.constraints
    const compareFieldValue = (args.object as any)[compareField]
    let isUnique

    if (compareField) {
      isUnique = await getRepository(repo)
        .createQueryBuilder('alias')
        .where(`alias.${key} = :id`, { id: value })
        .andWhere(`alias.id = :id`, { id: compareFieldValue })
        .withDeleted()
        .getOne()
    } else {
      isUnique = await getRepository(repo)
        .createQueryBuilder('alias')
        .where(`alias.${key} = :id`, { id: value })
        .withDeleted()
        .getOne()
    }

    return isUnique ? false : true
  }
}
