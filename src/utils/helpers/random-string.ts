export const randomString = (mask: string, length: number) => {
  let chars = ''

  if (mask.indexOf('a') > -1) chars += 'abcdefghijklmnopqrstuvwxyz'
  if (mask.indexOf('A') > -1) chars += 'ABCDEFGHIJKLMNOPQRSTUVWXYZ'
  if (mask.indexOf('0') > -1) chars += '0123456789'
  if (mask.indexOf('~') > -1) chars += '~`!@#$%^&*()_+-={}[]:";\'<>?,./|\\'

  let result = ''

  for (let i = length; i > 0; --i)
    result += chars[Math.round(Math.random() * (chars.length - 1))]

  return result
}
