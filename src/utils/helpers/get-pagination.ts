import { PaginationResponse } from '../responses/pagination.response'

export function paginationMetaData<T>(
  limit: number,
  page: number,
  total: number,
  data: T,
): PaginationResponse<T> {
  const checkpage = checkPage(total, { limit, page })

  return new PaginationResponse<T>(page, checkpage.lastPage, total, limit, data)
}

export function checkPagination(paginationArgs: {
  limit: number
  page: number
}) {
  const paginate: { take: number; skip: number } = {
    take: 10,
    skip: 0,
  }

  paginate.take = Number(paginationArgs.limit >= 0 ? paginationArgs.limit : 10)
  paginate.skip = Number(
    !paginationArgs.page || paginationArgs.page <= 1
      ? 0
      : paginationArgs.page * paginate.take - paginate.take,
  )

  return paginate
}

export function checkPage(
  count: number,
  paginate: { limit: number; page: number },
) {
  const limit_take = checkPagination(paginate)
  const page = {
    total: 0,
    currentPage: 0,
    lastPage: 1,
  }

  page.total = count
  page.lastPage =
    count == 0 || limit_take.take == 0
      ? 1
      : Math.ceil(page.total / limit_take.take)
  page.currentPage = limit_take.skip >= 1 ? paginate.page : 1

  return page
}
