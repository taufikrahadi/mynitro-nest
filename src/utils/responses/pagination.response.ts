import { ApiProperty } from '@nestjs/swagger'

export class PaginationResponse<T> {
  constructor(
    currentPage: number,
    lastPage: number,
    total: number,
    perPage: number,
    data: T,
  ) {
    this.currentPage = currentPage
    this.lastPage = lastPage
    this.total = total
    this.perPage = perPage
    this.data = data
  }
  @ApiProperty()
  currentPage: number
  @ApiProperty()
  lastPage: number
  @ApiProperty()
  total: number
  @ApiProperty()
  perPage: number
  @ApiProperty()
  data: T
}
