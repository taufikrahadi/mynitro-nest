import {
  Logger,
  ValidationPipe,
  ValidationError,
  BadRequestException,
} from '@nestjs/common'
import { ConfigService } from '@nestjs/config'
import { NestFactory } from '@nestjs/core'
import {
  NestFastifyApplication,
  FastifyAdapter,
} from '@nestjs/platform-fastify'
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger'
import * as moment from 'moment-timezone'
import { AppModule } from './application/app.module'

async function bootstrap() {
  const logger = new Logger()
  const app = await NestFactory.create<NestFastifyApplication>(
    AppModule,
    new FastifyAdapter(),
  )

  moment.tz.setDefault('Asia/Jakarta')
  app.setGlobalPrefix('/api')

  const swaggerConfig = new DocumentBuilder()
    .setTitle('MyNitro Jogja API Docs')
    .setDescription('Cloud API Documentation for MyNitro Jogja')
    .addBearerAuth()
    .addBasicAuth()
    .build()

  const document = SwaggerModule.createDocument(app, swaggerConfig)
  SwaggerModule.setup('docs', app, document)

  const configService = app.get<ConfigService>(ConfigService)
  const port = configService.get<number>('PORT') || 8080

  // use global validation pipe
  app.useGlobalPipes(
    new ValidationPipe({
      exceptionFactory: (errors: ValidationError[]) => {
        throw new BadRequestException(errors)
      },
    }),
  )

  await app.listen(port)
  logger.log(`Application is running on port ${port}`, 'NestApplication')
}
bootstrap()
