import { ApiProperty } from '@nestjs/swagger'
import * as moment from 'moment-timezone'
import {
  CreateDateColumn,
  DeleteDateColumn,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm'

export class BaseDomain {
  @PrimaryGeneratedColumn('uuid')
  @ApiProperty()
  id?: string

  @CreateDateColumn({
    transformer: {
      from(value) {
        return moment(value).format('YYYY MM DD HH:mm:ss')
      },
      to(value) {
        return value
      },
    },
  })
  @ApiProperty()
  createdAt?: Date

  @UpdateDateColumn({
    transformer: {
      from(value) {
        return moment(value).format('YYYY MM DD HH:mm:ss')
      },
      to(value) {
        return value
      },
    },
  })
  @ApiProperty()
  updatedAt?: Date

  @DeleteDateColumn()
  @ApiProperty()
  deletedAt?: Date
}
