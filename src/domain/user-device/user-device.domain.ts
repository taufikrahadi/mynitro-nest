import { Column, Entity, ManyToOne, PrimaryGeneratedColumn } from 'typeorm'
import { Device } from '../device/device.domain'
import { User } from '../user/user.domain'

@Entity()
export class UserDevice {
  @PrimaryGeneratedColumn('increment')
  id?: number

  @Column({
    type: 'int',
    nullable: false,
  })
  userId?: number

  @Column({
    type: 'int',
    nullable: false,
  })
  deviceId?: number

  @ManyToOne(() => User)
  user?: User

  @ManyToOne(() => Device, {
    eager: true,
  })
  device?: Device
}
