import { randomString } from '../../utils/helpers/random-string'
import { BeforeInsert, Column, Entity, ManyToOne } from 'typeorm'
import { BaseDomain } from '../base.domain'
import { Device } from '../device/device.domain'

@Entity()
export class Operator extends BaseDomain {
  @Column({
    type: 'varchar',
    nullable: false,
  })
  fullname: string

  @Column({
    type: 'varchar',
    nullable: false,
    unique: true,
  })
  phone: string

  @Column({
    type: 'varchar',
    length: 6,
    select: false,
  })
  pin?: string

  @Column({
    type: 'int',
    nullable: false,
    unsigned: true,
  })
  deviceId: number

  @ManyToOne(() => Device)
  device: Device

  @BeforeInsert()
  setPin?() {
    this.pin = randomString('0', 6)
  }
}
