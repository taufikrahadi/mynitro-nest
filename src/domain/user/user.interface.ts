export class UserToken {
  userId: string
  name: string
  email: string
  role: string
}
