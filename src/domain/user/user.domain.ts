import { BeforeInsert, Column, Entity, OneToMany } from 'typeorm'
import { BaseDomain } from '../base.domain'
import { hashSync, genSaltSync } from 'bcrypt'
import { UserDevice } from '../user-device/user-device.domain'

@Entity()
export class User extends BaseDomain {
  @Column({
    type: 'varchar',
    length: 100,
    nullable: false,
  })
  name: string

  @Column({
    type: 'varchar',
    unique: true,
    nullable: false,
  })
  email: string

  @Column({
    type: 'varchar',
    nullable: false,
  })
  phone: string

  @Column({
    type: 'varchar',
    nullable: false,
    select: false,
  })
  password: string

  @Column({
    type: 'varchar',
    nullable: false,
  })
  role: 'superadmin' | 'admin'

  @OneToMany(() => UserDevice, (userDevice) => userDevice.user, {
    cascade: ['insert', 'update', 'soft-remove', 'remove'],
    eager: true,
  })
  devices?: UserDevice[]

  @BeforeInsert()
  hashPassword?() {
    this.password = hashSync(this.password, genSaltSync(12))
  }
}
