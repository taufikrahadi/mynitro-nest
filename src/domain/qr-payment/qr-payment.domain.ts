import { BeforeInsert, Column, Entity, ManyToOne } from 'typeorm'
import { BaseDomain } from '../base.domain'
import * as moment from 'moment-timezone'
import { Payment } from '../payment/payment.domain'

@Entity()
export class QrPayment extends BaseDomain {
  @Column({
    type: 'uuid',
    nullable: false,
  })
  paymentId: string

  @Column({
    type: 'varchar',
    nullable: false,
  })
  host: string

  @Column({
    type: 'varchar',
    nullable: false,
  })
  tid: string

  @Column({
    type: 'varchar',
    nullable: false,
  })
  mid: string

  @Column({
    type: 'varchar',
    nullable: false,
  })
  reffNo: string

  @Column({
    type: 'varchar',
    nullable: false,
  })
  trxId: string

  @Column({
    type: 'varchar',
    nullable: false,
  })
  provider: string

  @Column({
    type: 'int',
    nullable: false,
    unsigned: true,
  })
  amount: number

  @Column({
    type: 'timestamp',
    transformer: {
      from: (val) => val,
      to: (val) => moment(val).tz('Asia/Jakarta').format('YYYY-MM-DD HH:mm:ss'),
    },
  })
  datetime: Date

  @Column()
  status: string

  @BeforeInsert()
  setReffNo?() {
    this.reffNo = `MYNITRO${moment.now()}${this.tid}`
  }

  @ManyToOne(() => Payment)
  payment: Payment
}
