import { ApiProperty } from '@nestjs/swagger'
import { randomString } from 'src/utils/helpers/random-string'
import { BeforeInsert, Column, Entity, JoinColumn, ManyToOne } from 'typeorm'
import { BaseDomain } from '../base.domain'
import { Device } from '../device/device.domain'

@Entity()
export class Order extends BaseDomain {
  @Column({
    type: 'varchar',
    unique: true,
    nullable: true,
  })
  @ApiProperty()
  orderNo?: string

  @Column({
    type: 'varchar',
    nullable: false,
  })
  @ApiProperty()
  orderType: string

  @Column({
    type: 'uuid',
    nullable: false,
  })
  @ApiProperty()
  deviceId: string

  @ApiProperty()
  @Column({
    type: 'varchar',
    nullable: false,
  })
  vehicleType: string | 'car' | 'bike'

  @Column({
    type: 'int',
    nullable: false,
    unsigned: true,
  })
  @ApiProperty()
  tireCount: number

  @Column({
    type: 'int',
    nullable: false,
    unsigned: true,
  })
  @ApiProperty()
  price: number

  @Column({
    type: 'varchar',
    nullable: false,
    default: 'waiting',
  })
  @ApiProperty()
  status: string

  @ManyToOne(() => Device, { eager: true })
  @JoinColumn({
    name: 'device_id',
    referencedColumnName: 'id',
  })
  @ApiProperty()
  device?: Device

  @BeforeInsert()
  setOrderNo?() {
    this.orderNo = `${randomString('0', 6)}-${randomString('a', 3)}`
  }
}
