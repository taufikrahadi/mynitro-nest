import { ApiProperty } from '@nestjs/swagger'
import { genSaltSync, hashSync } from 'bcrypt'
import { IsNotEmpty, IsNumber } from 'class-validator'
import {
  BeforeInsert,
  Column,
  Entity,
  JoinColumn,
  OneToMany,
  OneToOne,
} from 'typeorm'
import { BaseDomain } from '../base.domain'
import { Operator } from '../operator/operator.domain'
import { UserDevice } from '../user-device/user-device.domain'

export class PriceSet {
  @IsNumber()
  @IsNotEmpty()
  @ApiProperty()
  carNew: number

  @IsNumber()
  @ApiProperty()
  @IsNotEmpty()
  carRefill: number

  @IsNumber()
  @ApiProperty()
  @IsNotEmpty()
  carPatch: number

  @IsNumber()
  @ApiProperty()
  @IsNotEmpty()
  bikeNew: number

  @IsNumber()
  @ApiProperty()
  @IsNotEmpty()
  bikeRefill: number

  @IsNumber()
  @ApiProperty()
  @IsNotEmpty()
  bikePatch: number
}

@Entity()
export class Device extends BaseDomain {
  @Column({
    type: 'varchar',
    unique: true,
    nullable: false,
  })
  @ApiProperty()
  imei: string

  @Column({
    type: 'varchar',
    nullable: true,
  })
  @ApiProperty()
  name: string

  @Column({
    type: 'varchar',
    nullable: true,
  })
  @ApiProperty()
  address: string

  @Column({
    type: 'float',
    nullable: true,
  })
  @ApiProperty()
  latitude: number

  @Column({
    type: 'float',
    nullable: true,
  })
  @ApiProperty()
  longitude: number

  @Column({
    type: 'varchar',
    nullable: false,
    select: false,
  })
  /**
   * auto generated when device is accessing device auth endpoint
   */
  @ApiProperty()
  secretKey?: string

  @Column({
    type: 'varchar',
    nullable: true,
  })
  @ApiProperty()
  qrPaymentTid?: string

  @Column({
    type: 'json',
    nullable: true,
    transformer: {
      from: (val) => JSON.parse(val),
      to: (val) => val,
    },
  })
  @ApiProperty()
  priceSet?: PriceSet | string

  @Column({
    type: 'boolean',
    nullable: false,
    default: false,
  })
  @ApiProperty()
  emergencyPaymentStatus?: boolean

  @Column({
    type: 'int',
    nullable: true,
  })
  @ApiProperty()
  activeOperatorId?: number

  @OneToMany(() => UserDevice, (userDevice) => userDevice.device)
  users?: UserDevice[]

  @OneToMany(() => Operator, (operator) => operator.device)
  operator?: Operator[]

  @OneToOne(() => Operator)
  @JoinColumn({
    name: 'activeOperatorId',
    referencedColumnName: 'id',
  })
  activeOperator?: Operator

  @BeforeInsert()
  setPriceSet?() {
    this.priceSet = JSON.stringify(this.priceSet)
  }

  @BeforeInsert()
  setSecretKey?() {
    this.secretKey = hashSync(this.secretKey, genSaltSync(12))
  }
}
