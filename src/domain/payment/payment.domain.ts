import { ApiProperty } from '@nestjs/swagger'
import { Column, Entity, ManyToOne } from 'typeorm'
import { BaseDomain } from '../base.domain'
import { Order } from '../order/order.domain'

@Entity()
export class Payment extends BaseDomain {
  @Column({
    type: 'uuid',
    unsigned: true,
    nullable: false,
  })
  @ApiProperty()
  orderId: string

  @Column({
    type: 'int',
    unsigned: true,
    nullable: false,
  })
  @ApiProperty()
  amount: number

  @Column({
    type: 'enum',
    enum: ['cashless', 'cash'],
    nullable: false,
  })
  @ApiProperty()
  paymentType: 'cashless' | 'cash'

  @Column({
    type: 'varchar',
    nullable: false,
  })
  @ApiProperty()
  provider: string

  @Column({
    type: 'enum',
    enum: ['waiting', 'success', 'canceled'],
    nullable: false,
    default: 'waiting',
  })
  @ApiProperty()
  status?: string

  @ManyToOne(() => Order)
  order?: Order
}
