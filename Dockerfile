FROM node:16.13-buster as development-stage
WORKDIR /app

COPY package*.json /app
COPY yarn.lock /app
RUN yarn install
COPY . .

EXPOSE 8080

CMD [ "yarn", "start:dev" ]

FROM development-stage as build-stage
RUN yarn build

FROM build-stage as production-stage
COPY --from=build-stage /app/dist /usr/app
EXPOSE 8080

CMD [ "yarn", "start:prod" ]
